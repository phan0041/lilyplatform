from rest_framework import generics, permissions
from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Player, Game, GameLevel, Plan
from .models import PlayerGamePlay, PlayerGameDataRecord, UserGameFeedback


class LevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameLevel


class GameSerializer(serializers.ModelSerializer):
    levels = LevelSerializer(many=True)

    class Meta:
        model = Game
        fields = ('id', 'uuid', 'name', 'levels')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')


class PlayerFullSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=False)

    class Meta:
        model = Player


class PlayerSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(slug_field='pk', read_only=True)
    role = serializers.CharField(read_only=True)

    class Meta:
        model = Player


class PlaySerializer(serializers.ModelSerializer):
    game_level = serializers.SlugRelatedField(slug_field='uuid')
    plan = serializers.SlugRelatedField(slug_field='uuid', required=False)
    player = PlayerSerializer(required=False)

    class Meta:
        model = PlayerGamePlay


class DataRecordSerializer(serializers.ModelSerializer):
    game_play = serializers.SlugRelatedField(slug_field='uuid')

    class Meta:
        model = PlayerGameDataRecord


class FeedbackSerializer(serializers.ModelSerializer):
    game = serializers.SlugRelatedField(slug_field='uuid')
    player = PlayerSerializer(required=False)

    class Meta:
        model = UserGameFeedback


class PlanSerializer(serializers.ModelSerializer):
    # game = serializers.SlugRelatedField(slug_field='pk')
    # player = serializers.SlugRelatedField(slug_field='pk')

    class Meta:
        model = Plan


class UserDetailsSerializer(serializers.ModelSerializer):
    user_info = serializers.SerializerMethodField('get_user_info')

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'user_info')

    def get_user_info(self, obj):
        extra = None
        try:
            userprofile = obj.get_profile()
            player = None
            if userprofile.user_type == 'player':
                player = Player.objects.get(user=obj)
                serializer = PlayerSerializer(player)
                player = serializer.data
                extra = {
                    'user_type': 'player',
                    'player': player
                }
        except:
            pass
        return extra
