import json
from pprint import pprint
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from .models import Game, GameLevel, GameSeries
from .models import Player, Staff, Organization
from .models import Plan, UserGameFeedback
from .models import PlayerGamePlay


class BaseAPITestCase(APITestCase):
    def setUp(self):
        self.USERNAME = 'player1'
        self.PASSWORD = '1234'

        self.org = Organization.objects.create(name="hospital 1")
        self.staff_user = User.objects.create_user('doctor1', '', '1234')
        self.staff_user.userprofile.user_type = 'staff'
        self.staff = Staff.objects.create(user=self.staff_user,
                                          organization=self.org,
                                          role='doctor')

        self.user = User.objects.create_user(self.USERNAME, '', self.PASSWORD)
        self.token = Token.objects.get(user__username=self.user.username)
        self.player = Player.objects.create(user=self.user)

        self.gameseries = GameSeries.objects.create(name="first series")
        self.game = Game.objects.create(name='first game',
                                        series=self.gameseries)
        self.level1 = GameLevel.objects.create(game=self.game, name='level 1')
        self.level2 = GameLevel.objects.create(game=self.game, name='level 2')

        self.plan1 = Plan.objects.create(name='first plan',
                                         game=self.game,
                                         staff=self.staff,
                                         player=self.player)

        self.feedback1 = UserGameFeedback.objects.create(game=self.game,
                                                         rank=2,
                                                         player=self.player)

        self.gameplay1 = PlayerGamePlay.objects.create(player=self.player,
                                                       game_level=self.level1,
                                                       plan=self.plan1,
                                                       play_time="2013-01-29 23:34:34")

    def test_login(self):
        payload = {
            "username": self.USERNAME,
            "password": self.PASSWORD
        }
        login_url = reverse('rest_login')
        response = self.client.post(login_url, data=payload)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual('key' in response.keys(), True)

    def test_get_userinfo(self):
        userinfo_url = reverse('rest_user_details')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.get(userinfo_url)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual(response['username'], self.user.username)

    def test_get_userdetails(self):
        user_detail_url = reverse('user-detail', kwargs={
            'username': self.user.username})
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.get(user_detail_url)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual(response['username'], self.user.username)
        self.assertEqual('user_info' in response, True)
        self.assertEqual('player' in response['user_info'], True)

    def test_update_player(self):
        payload = {
            "address": 'singapore',
            # 'user': 200,
            'role': 'testrole',
        }
        player_detail_url = reverse('player-detail',
                                    kwargs={'pk': self.player.pk})
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.patch(player_detail_url, data=payload)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual(response['address'], payload['address'])
        # self.assertNotEqual(response['user'], payload['user'])
        self.assertNotEqual(response['role'], payload['role'])

    def test_get_game(self):
        game_detail_url = reverse('game-detail',
                                  kwargs={'uuid': self.game.uuid})
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.get(game_detail_url)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual(response['name'], self.game.name)

    def test_get_playergameplans(self):
        plan_list_url = reverse('playergameplan-list', kwargs={
            'pk': self.player.pk,
            'game_uuid': self.game.uuid
            })
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.get(plan_list_url)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual(len(response), 1)

    def test_get_playergamefeedbacks(self):
        feedback_list_url = reverse('playergamefeedback-list', kwargs={
            'pk': self.player.pk,
            'game_uuid': self.game.uuid
            })
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.get(feedback_list_url)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual(len(response), 1)

    def test_create_gameplay(self):
        payload = {
            "game_level": self.level1.uuid,
            "plan": self.plan1.uuid,
            "play_time": "2013-01-29 23:34:34",
        }
        play_list_url = reverse('play-list')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.post(play_list_url, data=payload)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual(response['game_level'], payload['game_level'])
        self.assertEqual(response['plan'], payload['plan'])

    def test_create_datarecord(self):
        payload = {
            "game_play": self.gameplay1.uuid,
            "data": "data",
            'data_type': 'behavior',
        }
        datarecord_list_url = reverse('datarecord-list')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.post(datarecord_list_url, data=payload)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual(response['game_play'], payload['game_play'])

    def test_create_feedback(self):
        payload = {
            "game": self.game.uuid,
            "rank": 4
        }
        feedback_list_url = reverse('feedback-list')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.post(feedback_list_url, data=payload)
        response = json.loads(response.content)
        pprint(response)
        self.assertEqual(response['game'], payload['game'])
