# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'DiseaseStage'
        db.create_table(u'dataapp_diseasestage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('disease', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.Disease'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['DiseaseStage'])

        # Adding model 'Measure'
        db.create_table(u'dataapp_measure', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('staff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.Staff'])),
            ('game_level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.GameLevel'])),
            ('type_limit', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('threshold', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('direction', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['Measure'])

        # Adding model 'Staff'
        db.create_table(u'dataapp_staff', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='staff_profile', unique=True, to=orm['auth.User'])),
            ('organization', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.Organization'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['Staff'])

        # Adding model 'GameSeries'
        db.create_table(u'dataapp_gameseries', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['GameSeries'])

        # Adding model 'Organization'
        db.create_table(u'dataapp_organization', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['Organization'])

        # Adding model 'UserProfile'
        db.create_table(u'dataapp_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='userprofile', unique=True, to=orm['auth.User'])),
            ('user_type', self.gf('django.db.models.fields.CharField')(default='player', max_length=30)),
        ))
        db.send_create_signal(u'dataapp', ['UserProfile'])

        # Adding model 'StaffRelationship'
        db.create_table(u'dataapp_staffrelationship', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nurse', self.gf('django.db.models.fields.related.ForeignKey')(related_name='nurse', to=orm['dataapp.Staff'])),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(related_name='doctor', to=orm['dataapp.Staff'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['StaffRelationship'])

        # Adding model 'Game'
        db.create_table(u'dataapp_game', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
            ('series', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.GameSeries'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['Game'])

        # Adding M2M table for field organizations on 'Game'
        m2m_table_name = db.shorten_name(u'dataapp_game_organizations')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('game', models.ForeignKey(orm[u'dataapp.game'], null=False)),
            ('organization', models.ForeignKey(orm[u'dataapp.organization'], null=False))
        ))
        db.create_unique(m2m_table_name, ['game_id', 'organization_id'])

        # Adding model 'PlayerGamePlay'
        db.create_table(u'dataapp_playergameplay', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('player', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.Player'])),
            ('game_level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.GameLevel'])),
            ('plan', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.Plan'], null=True, blank=True)),
            ('play_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, null=True, blank=True)),
            ('extra_data', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('score', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['PlayerGamePlay'])

        # Adding model 'GameLevel'
        db.create_table(u'dataapp_gamelevel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.Game'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['GameLevel'])

        # Adding model 'PlayerGameDataRecord'
        db.create_table(u'dataapp_playergamedatarecord', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('game_play', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.PlayerGamePlay'])),
            ('data', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('is_checked', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['PlayerGameDataRecord'])

        # Adding model 'Plan'
        db.create_table(u'dataapp_plan', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.Game'])),
            ('player', self.gf('django.db.models.fields.related.ForeignKey')(related_name='player', to=orm['dataapp.Player'])),
            ('staff', self.gf('django.db.models.fields.related.ForeignKey')(related_name='staff', to=orm['dataapp.Staff'])),
            ('start_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now, null=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now, null=True, blank=True)),
            ('plan_data', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['Plan'])

        # Adding model 'Player'
        db.create_table(u'dataapp_player', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='player_profile', unique=True, to=orm['auth.User'])),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('birthdate', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(1950, 1, 1, 0, 0), null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('age_group', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['Player'])

        # Adding M2M table for field doctors on 'Player'
        m2m_table_name = db.shorten_name(u'dataapp_player_doctors')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('player', models.ForeignKey(orm[u'dataapp.player'], null=False)),
            ('staff', models.ForeignKey(orm[u'dataapp.staff'], null=False))
        ))
        db.create_unique(m2m_table_name, ['player_id', 'staff_id'])

        # Adding M2M table for field disease_stages on 'Player'
        m2m_table_name = db.shorten_name(u'dataapp_player_disease_stages')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('player', models.ForeignKey(orm[u'dataapp.player'], null=False)),
            ('diseasestage', models.ForeignKey(orm[u'dataapp.diseasestage'], null=False))
        ))
        db.create_unique(m2m_table_name, ['player_id', 'diseasestage_id'])

        # Adding model 'Disease'
        db.create_table(u'dataapp_disease', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['Disease'])

        # Adding model 'UserGameFeedback'
        db.create_table(u'dataapp_usergamefeedback', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['dataapp.Game'])),
            ('rank', self.gf('django.db.models.fields.IntegerField')()),
            ('comments', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'dataapp', ['UserGameFeedback'])


    def backwards(self, orm):
        # Deleting model 'DiseaseStage'
        db.delete_table(u'dataapp_diseasestage')

        # Deleting model 'Measure'
        db.delete_table(u'dataapp_measure')

        # Deleting model 'Staff'
        db.delete_table(u'dataapp_staff')

        # Deleting model 'GameSeries'
        db.delete_table(u'dataapp_gameseries')

        # Deleting model 'Organization'
        db.delete_table(u'dataapp_organization')

        # Deleting model 'UserProfile'
        db.delete_table(u'dataapp_userprofile')

        # Deleting model 'StaffRelationship'
        db.delete_table(u'dataapp_staffrelationship')

        # Deleting model 'Game'
        db.delete_table(u'dataapp_game')

        # Removing M2M table for field organizations on 'Game'
        db.delete_table(db.shorten_name(u'dataapp_game_organizations'))

        # Deleting model 'PlayerGamePlay'
        db.delete_table(u'dataapp_playergameplay')

        # Deleting model 'GameLevel'
        db.delete_table(u'dataapp_gamelevel')

        # Deleting model 'PlayerGameDataRecord'
        db.delete_table(u'dataapp_playergamedatarecord')

        # Deleting model 'Plan'
        db.delete_table(u'dataapp_plan')

        # Deleting model 'Player'
        db.delete_table(u'dataapp_player')

        # Removing M2M table for field doctors on 'Player'
        db.delete_table(db.shorten_name(u'dataapp_player_doctors'))

        # Removing M2M table for field disease_stages on 'Player'
        db.delete_table(db.shorten_name(u'dataapp_player_disease_stages'))

        # Deleting model 'Disease'
        db.delete_table(u'dataapp_disease')

        # Deleting model 'UserGameFeedback'
        db.delete_table(u'dataapp_usergamefeedback')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'dataapp.disease': {
            'Meta': {'object_name': 'Disease'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'dataapp.diseasestage': {
            'Meta': {'object_name': 'DiseaseStage'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'disease': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.Disease']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'dataapp.game': {
            'Meta': {'object_name': 'Game'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'organizations': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['dataapp.Organization']", 'null': 'True', 'blank': 'True'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.GameSeries']"})
        },
        u'dataapp.gamelevel': {
            'Meta': {'ordering': "['order']", 'object_name': 'GameLevel'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'dataapp.gameseries': {
            'Meta': {'object_name': 'GameSeries'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'dataapp.measure': {
            'Meta': {'object_name': 'Measure'},
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'game_level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.GameLevel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.Staff']"}),
            'threshold': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'type_limit': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'dataapp.organization': {
            'Meta': {'object_name': 'Organization'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'dataapp.photo': {
            'Meta': {'object_name': 'Photo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['dataapp.Post']"})
        },
        u'dataapp.plan': {
            'Meta': {'object_name': 'Plan'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'plan_data': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'player'", 'to': u"orm['dataapp.Player']"}),
            'staff': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'staff'", 'to': u"orm['dataapp.Staff']"}),
            'start_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'})
        },
        u'dataapp.player': {
            'Meta': {'object_name': 'Player'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'age_group': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'birthdate': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1950, 1, 1, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'disease_stages': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['dataapp.DiseaseStage']", 'null': 'True', 'blank': 'True'}),
            'doctors': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'doctors'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['dataapp.Staff']"}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'player_profile'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'dataapp.playergamedatarecord': {
            'Meta': {'object_name': 'PlayerGameDataRecord'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'game_play': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.PlayerGamePlay']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_checked': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'dataapp.playergameplay': {
            'Meta': {'object_name': 'PlayerGamePlay'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'extra_data': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'game_level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.GameLevel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.Plan']", 'null': 'True', 'blank': 'True'}),
            'play_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.Player']"}),
            'score': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'})
        },
        u'dataapp.post': {
            'Meta': {'object_name': 'Post'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'posts'", 'to': u"orm['auth.User']"}),
            'body': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'dataapp.profile': {
            'Meta': {'object_name': 'Profile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'profile'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'dataapp.profilephoto': {
            'Meta': {'object_name': 'ProfilePhoto'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['dataapp.Profile']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'})
        },
        u'dataapp.staff': {
            'Meta': {'object_name': 'Staff'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.Organization']"}),
            'relationships': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'assist'", 'symmetrical': 'False', 'through': u"orm['dataapp.StaffRelationship']", 'to': u"orm['dataapp.Staff']"}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'staff_profile'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'dataapp.staffrelationship': {
            'Meta': {'object_name': 'StaffRelationship'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'doctor'", 'to': u"orm['dataapp.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nurse': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'nurse'", 'to': u"orm['dataapp.Staff']"})
        },
        u'dataapp.usergamefeedback': {
            'Meta': {'object_name': 'UserGameFeedback'},
            'comments': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dataapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rank': ('django.db.models.fields.IntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'dataapp.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'userprofile'", 'unique': 'True', 'to': u"orm['auth.User']"}),
            'user_type': ('django.db.models.fields.CharField', [], {'default': "'player'", 'max_length': '30'})
        }
    }

    complete_apps = ['dataapp']