from __future__ import absolute_import
from django.conf.urls import patterns, include, url
from .views import home_view, player_game_view
from .views import profile, blogpost, upload_image, apidoc
from .routers import router as api_router
from .blogpost_urls import user_urls, post_urls, photo_urls, blogpost_urls
from .game_urls import gameplatform_urls


urlpatterns = patterns('',
    url(r'^$', view=home_view, name='home'),
    url(r'^players/(?P<player_pk>\d+)/games/(?P<game_uuid>[0-9a-zA-Z_-]+)/$', view=player_game_view, name='player_game_view'),

    url(r'^profile/$', view=profile, name='profile-page'),
    url(r'^blogpost/$', view=blogpost, name='blogpost-page'),

    # APIs
    url(r'^apidoc/', view=apidoc, name='apidoc'),
    url(r'^api/', include(gameplatform_urls)),
    url(r'^rest-auth/', include('rest_auth.urls')),

    # samples
    url(r'^profile-api/', include(api_router.urls)),
    url(r'^upload/', view=upload_image, name='photo-upload'),
    url(r'^blogpost-api/', include(blogpost_urls)),
)
