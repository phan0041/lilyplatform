import math
import pdb
from dataapp.models import PlayerGamePlay, PlayerGameDataRecord

def task_vis(player_id, gamelevel_uuid):
    """
        player_id is the primary key in Player Table
        obtain the data needed for the virtualization from the db by yourself
    """
    #dummy
    # sin = []
    # other = []
    # cos = []
    # for i in range(20):
    #     sin.append({'x': i, 'y': math.sin(float(i)/10)})
    #     cos.append({'x': i, 'y': 0.5 * math.cos(float(i)/10)})
    #     other.append({'x': i, 'y': 0.2 * math.sin(float(i)/10)})
    #
    # ret_data = [
    #     {'values': sin, 'key': 'Memory Accuracy', 'color': '#ff7f0e'},
    #     {'values': cos, 'key': 'Speed of cognition', 'color': '#2ca02c'},
    #     {'values': other, 'key': 'Game completion'}]
    #enddummy
    ret_data = 'testtesttestthisispreprocesseddata'
    #get task. Queryset of Multple gameplay
    playergameplay = PlayerGamePlay.objects.filter(player__pk=player_id).filter(game_level__uuid=gamelevel_uuid)

    # process
    #user_scores = preproc_allplay(playergameplay)

    return ret_data


def play_vis(play_id):
    """
        play_id is the primary key in PlayerGamePlay Table
        obtain the data needed for the virtualization from the db by yourself
    """
    ret_data = {'hello': 'play vis'}
    play = PlayerGamePlay.objects.get(pk=play_id)

    # process
    #user_score = preproc_singleplay(gameplay)
    ret_data = 'testtesttestthisispreprocesseddata'
    return ret_data

class RecordEventAdapter:
    """play_id
    Adapter to convert Record data type to Event data type
    TODO
    """
    def __init__(self, record):
        self._record = record

    def get(self, event_name="null"):
        return

    def filter(self, event_name="null"):
        return

def get_scores(gameplays):
    user_memory_scores = []
    pm_flip_memory_scores = []
    pm_fairy_memory_scores = []
    pm_fisherman_memory_scores = []
    user_time_scores = []
    user_collect_scores = []
    for play in gameplays:
        records = PlayerGameDataRecord.objects.filter(game_play=play)
        events = RecordEventAdapter(records)
        try:
            scores = get_user_metrics(events)
            user_memory_scores.append(scores['pm_memory_scores']['pm_memory_score'])
            pm_flip_memory_scores.append(scores['pm_memory_scores']['pm_flip_memory_score'])
            pm_fairy_memory_scores.append(scores['pm_memory_scores']['pm_fairy_memory_score'])
            pm_fisherman_memory_scores.append(scores['pm_memory_scores']['pm_fisherman_memory_score'])
            user_time_scores.append(scores['user_total_time_score'])
            user_collect_scores.append(scores['user_total_collect_score'])
        except:
            pass
    user_scores = {
        "user_memory_scores": user_memory_scores,
        "pm_flip_memory_scores": pm_flip_memory_scores,
        "pm_fairy_memory_scores": pm_fairy_memory_scores,
        "pm_fisherman_memory_scores": pm_fisherman_memory_scores,
        "user_time_scores": user_time_scores,
        "user_collect_scores": user_collect_scores
    }
    return user_scores


def preproc_singleplay(gameplay):
    """
    Pre process data on 1 play of 1 user
    """
    return


def preproc_allplay(gameplays):
    """
    Pre-process all play from 1 game of 1 user
    """
    return get_scores(gameplays)


def preproc_alluser(gameplays):
    """
    Pre-Process all plays from all users
    """
    # user_avg_memory_scores = []
    # user_avg_time_scores = []
    # user_avg_collect_scores = []
    # user_overall_scores = []
    # for patient in patients:
    #     gameplays = GamePlay.objects.filter(patient=patient)
    #     if gameplays.count() > 0:
    #         user_scores = get_scores(gameplays)
    #         user_memory_scores, user_time_scores, user_collect_scores = user_scores['user_memory_scores'], user_scores['user_time_scores'], user_scores['user_collect_scores']
    #         user_avg_memory_score = sum(user_memory_scores) / len(user_memory_scores) if len(user_memory_scores) > 0 else 0.0
    #         user_avg_time_score = sum(user_time_scores) / len(user_time_scores) if len(user_time_scores) > 0 else 0.0
    #         user_avg_collect_score = sum(user_collect_scores) / len(user_collect_scores) if len(user_collect_scores) > 0 else 0.0
    #         user_overall_score = (user_avg_memory_score+user_avg_time_score+user_avg_collect_score)/3.0
    #         user_avg_memory_scores.append(user_avg_memory_score)
    #         user_avg_time_scores.append(user_avg_time_score)
    #         user_avg_collect_scores.append(user_avg_collect_score)
    #         user_overall_scores.append(user_overall_score)
    #
    return

''' utils functions '''
baseline_total_time = 60.0 * 5

baseline_fish_count = 6.0
baseline_pedal_count = 6.0

pm_flip_expected_time = 30.0
pm_fairy_expected_time = 30.0
pm_fisherman_expected_time = 30.0

algorithm = 1

def get_memory_performance(begin_time, exp_time, user_act_time, end_time, algorithm=0):
    begin_time, exp_time, user_act_time, end_time = 1.0, 10.0, 8.0, 12.0

    if (begin_time>exp_time) or (begin_time>user_act_time) or (end_time<exp_time) or (end_time<user_act_time):
        print "The input time information is wrong, please the system setting."
        return None

    if algorithm==0:
        score = (1-min(abs(exp_time-user_act_time)/float(exp_time-begin_time), 1)) * 100

    elif algorithm==1:
        sigma=(exp_time-begin_time)/3.291
        score = math.exp(-(user_act_time-exp_time)**2/(2*sigma**2)) * 100

    score = int(score)

    return score


def get_pmtask_memory_performance(events, start_event_name, end_event_name, user_event_name, expected_time):
    pm_start_event = events.get(event_name=start_event_name)
    pm_end_event = events.get(event_name=end_event_name)
    try:
        pm_user_event = events.get(event_name=user_event_name)
        user_act_time = (pm_user_event.event_time - pm_start_event.event_time).total_seconds()
        end_time = (pm_end_event.event_time - pm_start_event.event_time).total_seconds()
        user_pm_score = get_memory_performance(0.0, expected_time, user_act_time, end_time, algorithm)
    except:
        print 'EXCEPTION with [%s-%s-%s]' % (start_event_name, user_event_name, end_event_name)
        user_pm_score = 0
    return user_pm_score


def get_user_metrics(events):
    # import pdb; pdb.set_trace()
    start_game_event = events.get(event_name='Start Game')
    end_game_event = events.filter(event_name='End Game').order_by('-event_time')[0]
    collect_fish_event = events.filter(event_name='Dried Fish Collection')
    collect_pedal_event = events.filter(event_name='Dried Petal Collection')

    # user total time score
    total_time = end_game_event.event_time - start_game_event.event_time
    user_total_time = total_time.total_seconds()
    user_total_time_score = 100 if user_total_time > baseline_total_time else 100 * (user_total_time / baseline_total_time)

    # use total collection score
    user_total_fish = collect_fish_event.count()
    user_total_pedal = collect_pedal_event.count()
    user_total_fish_score = 100 if user_total_fish > baseline_fish_count else 100 * (user_total_fish / baseline_fish_count)
    user_total_pedal_score = 100 if user_total_pedal > baseline_pedal_count else 100 * (user_total_pedal / baseline_pedal_count)
    user_total_collect_score = (user_total_fish_score + user_total_pedal_score) / 2.0

    pm_flip_memory_score = get_pmtask_memory_performance(events, 'Flip_Task_1_Started', 'Flip_Task_1_Ended', 'Flip_Task_1', pm_flip_expected_time)
    pm_fairy_memory_score = get_pmtask_memory_performance(events, 'Gift_Fairy_Task_1_Started', 'Gift_Fairy_Task_1_Ended', 'Gift_Fairy_Task_1', pm_fairy_expected_time)
    pm_fisherman_memory_score = get_pmtask_memory_performance(events, 'Gift_Fisherman_Task_1_Started', 'Gift_Fisherman_Task_1_Ended', 'Gift_Fisherman_Task_1', pm_fisherman_expected_time)
    pm_memory_score = (pm_flip_memory_score + pm_fairy_memory_score + pm_fisherman_memory_score) / 3.0

    pm_memory_score, pm_fairy_memory_score, pm_fairy_memory_score, pm_fisherman_memory_score, user_total_time_score, user_total_collect_score = map(int, [pm_memory_score, pm_fairy_memory_score, pm_fairy_memory_score, pm_fisherman_memory_score, user_total_time_score, user_total_collect_score])

    scores = {
        "pm_memory_scores": {
            "pm_memory_score": pm_memory_score,
            "pm_flip_memory_score": pm_flip_memory_score,
            "pm_fairy_memory_score": pm_fairy_memory_score,
            "pm_fisherman_memory_score": pm_fisherman_memory_score,
        },
        "user_total_time_score": user_total_time_score,
        "user_total_collect_score": user_total_collect_score
    }
    return scores

