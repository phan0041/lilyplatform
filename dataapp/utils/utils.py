from importlib import import_module
from dataapp.models import PlayerGamePlay

gamelevel_function_dict = {
    'f4a6a3a1-df53-428f-a4da-29c3c2c7c1a9': 'dataapp.utils.parkinson.task_vis',
}


play_function_dict = {
    'f4a6a3a1-df53-428f-a4da-29c3c2c7c1a9': 'dataapp.utils.parkinson.play_vis',
    #'gameuuid':''
}


def load_module(module_str):
    module_str, vis_func_str = module_str.rsplit('.', 1)
    module = import_module(module_str)
    vis_func = getattr(module, vis_func_str)
    return vis_func


def all_session_visualize(player_id, gamelevel_uuid):
    vis_func = load_module(gamelevel_function_dict[gamelevel_uuid])

    ret_data = vis_func(player_id, gamelevel_uuid)

    return ret_data


def single_session_visualize(play_id):
    play = PlayerGamePlay.objects.get(pk=play_id)
    gamelevel_uuid = play.game_level.uuid

    vis_func = load_module(play_function_dict[gamelevel_uuid])

    ret_data = vis_func(play_id)

    return ret_data
