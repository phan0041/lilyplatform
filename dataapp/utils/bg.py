from dataapp.models import *


def task_vis(player_id, gamelevel_uuid):
    """
        player_id is the primary key in Player Table
        obtain the data needed for the virtualization from the db by yourself
    """
    ret_data = {'hello': 'task vis'}
    return ret_data


def play_vis(play_id):
    """
        play_id is the primary key in PlayerGamePlay Table
        obtain the data needed for the virtualization from the db by yourself
    """
    ret_data = {'hello': 'play vis'}
    return ret_data
