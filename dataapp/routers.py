from django.conf.urls import url
from rest_framework import viewsets, routers, serializers
from rest_framework import views, reverse, response
from .profile_api import ProfileViewSet, ProfilePhotoViewSet

router = routers.DefaultRouter()


router.register(r'profile', ProfileViewSet)
router.register(r'photo', ProfilePhotoViewSet)
