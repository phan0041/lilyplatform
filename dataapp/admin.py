from django.contrib import admin
from .models import Profile, ProfilePhoto
from .models import Post, Photo

from .models import Game, GameLevel, GameSeries
from .models import UserProfile
from .models import Organization, Staff, StaffRelationship
from .models import Player, Plan, PlayerGamePlay, PlayerGameDataRecord, UserGameFeedback

admin.site.register(Profile)
admin.site.register(ProfilePhoto)

admin.site.register(Post)
admin.site.register(Photo)

admin.site.register(UserProfile)
admin.site.register(Organization)
admin.site.register(Staff)
admin.site.register(StaffRelationship)
admin.site.register(Player)
admin.site.register(GameSeries)
admin.site.register(UserGameFeedback)
admin.site.register(PlayerGameDataRecord)


class GameAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
admin.site.register(Game, GameAdmin)


class GameLevelAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
admin.site.register(GameLevel, GameLevelAdmin)


class PlayerGamePlayAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
admin.site.register(PlayerGamePlay, PlayerGamePlayAdmin)

class PlanAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
admin.site.register(Plan, PlanAdmin)
