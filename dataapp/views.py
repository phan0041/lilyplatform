import os
from StringIO import StringIO
from django.conf import settings
from django.core.files.images import ImageFile
from django.shortcuts import render, redirect
from django.utils import simplejson
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from .models import ProfilePhoto
from .models import Player, Staff, Game
from .models import GENDER_CHOICES, PLAYER_ROLE_CHOICES, AGEGROUP_CHOICES


def home_view(request):
    if not request.user.is_authenticated():
        return render_to_response("homepage.html", {
            }, RequestContext(request))

    if request.user.userprofile.user_type == 'staff':
        staff = get_object_or_404(Staff, user=request.user)
        staff_org = staff.organization
        org_games = staff_org.game_set.all()
        staff_players = Player.objects.filter(doctors__in=[staff, ])
        return render_to_response("dataapp/dashboard.html", {
            'org_games': org_games,
            'gender_choices': GENDER_CHOICES,
            'player_role_choices': PLAYER_ROLE_CHOICES,
            'agegroup_choices': AGEGROUP_CHOICES,
            'staff_players': staff_players,
            }, RequestContext(request))

    return render_to_response("dataapp/error.html", {
        }, RequestContext(request))


@login_required
def player_game_view(request, player_pk, game_uuid):
    player = get_object_or_404(Player, pk=player_pk)
    game = get_object_or_404(Game, uuid=game_uuid)
    return render_to_response("dataapp/playergamepage.html", {
        'player': player,
        'game': game,
        }, RequestContext(request))


def apidoc(request):
    return render_to_response("dataapp/apidoc.html", {
        }, RequestContext(request))


def profile(request):
    if request.user.is_authenticated():
        return render_to_response("dataapp/profile.html", {
            'profile': request.user.get_profile()
            }, RequestContext(request))
    return redirect('admin:index')


def blogpost(request):
    if request.user.is_authenticated():
        return render_to_response("dataapp/blogpost.html", {
            }, RequestContext(request))
    return redirect('admin:index')


@csrf_exempt
def upload_image(request):
    user = request.user
    if (request.method != 'POST' or not request.is_ajax() or
        not user.is_authenticated()):
        raise Http404
    fname = request.GET.get('qqfile', '')
    try:
        fname = '.'.join(fname.split('.')[-2:])
    except IndexError:
        return HttpResponse(
            simplejson.dumps({'success': False}),
            mimetype='application/json')
    f = StringIO()
    while True:
        buf = request.read(512 * 1024)
        if buf:
            f.write(buf)
        else:
            break
    profile = user.get_profile()
    pf = ProfilePhoto(
            profile=profile,
            title=fname.split('.')[0],
            image=ImageFile(f, name=fname))
    pf.save()
    return HttpResponse(
        simplejson.dumps({'success': True, 'url': pf.image.url}),
        mimetype='application/json')
