from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.humanize.templatetags.humanize import naturaltime

from rest_framework.authtoken.models import Token
from django_extensions.db.fields import UUIDField

from sorl.thumbnail import ImageField


GENDER_CHOICES = (
    ('Male', 'Male'),
    ('Female', 'Female'),
    )


STAFF_ROLE_CHOICES = (
    ('doctor', 'doctor'),
    ('nurse', 'nurse'),
    ('admin', 'admin'),
    )

PLAYER_ROLE_CHOICES = (
    ('patient', 'patient'),
    ('nonpatient', 'nonpatient'),
    )

AGEGROUP_CHOICES = (
    ('30-40', '30-40'),
    ('40-50', '40-50'),
    )

USERROLE_CHOICES = (
    ('staff', 'staff'),
    ('player', 'player'),
    )


class Organization(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, blank=True, null=True)
    phone = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=500, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Disease(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, blank=True, null=True)

    def __unicode__(self):
        return self.name


class DiseaseStage(models.Model):
    disease = models.ForeignKey(Disease)
    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=1000, blank=True, null=True)

    def __unicode__(self):
        return '%d [%s]' % (self.disease, self.name)


class GameSeries(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Game(models.Model):
    name = models.CharField(max_length=100)
    uuid = UUIDField()
    description = models.CharField(max_length=1000, blank=True, null=True)
    image = ImageField(upload_to='gameimages', blank=True, null=True)
    series = models.ForeignKey(GameSeries)
    organizations = models.ManyToManyField(Organization, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return 'game [%s]' % self.name


class GameLevel(models.Model):
    game = models.ForeignKey(Game, related_name='levels')
    name = models.CharField(max_length=100)
    uuid = UUIDField()
    description = models.CharField(max_length=1000, blank=True, null=True)
    order = models.IntegerField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return '%s - %s' % (self.game.name, self.name)


class Staff(models.Model):
    user = models.OneToOneField(User, related_name='staff_profile')
    organization = models.ForeignKey(Organization)
    specialty = models.CharField(max_length=100, blank=True, null=True)
    department = models.CharField(max_length=100, blank=True, null=True)
    phone = models.CharField(max_length=50, blank=True, null=True)
    fax = models.CharField(max_length=50, blank=True, null=True)

    relationships = models.ManyToManyField('self', through='StaffRelationship', symmetrical=False, related_name='assist')
    role = models.CharField(max_length=32, choices=STAFF_ROLE_CHOICES, blank=True, null=True)

    def __unicode__(self):
        return "%s %s at %s" % (self.role, self.user, self.organization)


class StaffRelationship(models.Model):
    nurse = models.ForeignKey(Staff, related_name='nurse')
    doctor = models.ForeignKey(Staff, related_name='doctor')
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s assists %s" % (self.nurse, self.doctor)


class Player(models.Model):
    user = models.OneToOneField(User, related_name='player_profile')
    role = models.CharField(max_length=32, choices=PLAYER_ROLE_CHOICES, blank=True, null=True)
    doctors = models.ManyToManyField(Staff, blank=True, null=True, related_name='doctors')
    disease_stages = models.ManyToManyField(DiseaseStage, blank=True, null=True)
    sympton = models.TextField(blank=True, null=True)
    gender = models.CharField(max_length=32, choices=GENDER_CHOICES, blank=True, null=True)
    birthdate = models.DateField(blank=True, null=True, default= datetime.strptime('1950-01-01', '%Y-%m-%d'))
    #birthdate = models.DateField(blank=True, null=True, default='1950-01-01')
    phone = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=500, blank=True, null=True)
    language = models.CharField(max_length=100, blank=True, null=True)
    age_group = models.CharField(max_length=32, choices=AGEGROUP_CHOICES, blank=True, null=True)

    def __unicode__(self):
        return "Player %s" % (self.user.username)


class Plan(models.Model):
    name = models.CharField(max_length=100)
    uuid = UUIDField()
    game = models.ForeignKey(Game)
    player = models.ForeignKey(Player, related_name='player')
    staff = models.ForeignKey(Staff, related_name='staff')
    start_date = models.DateField(default=datetime.now, blank=True, null=True)
    end_date = models.DateField(default=datetime.now, blank=True, null=True)
    plan_data = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return 'Plan [%s] [%s]' % (self.name, self.player.user.username)


class Measure(models.Model):
    staff = models.ForeignKey(Staff)
    game_level = models.ForeignKey(GameLevel)
    type_limit = models.CharField(max_length=100, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)  # accuracy
    threshold = models.CharField(max_length=100, blank=True, null=True)
    direction = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return '%s - %s [%s]' % (self.name, self.threshold, self.direction)


class PlayerGamePlay(models.Model):
    uuid = UUIDField()
    player = models.ForeignKey(Player)
    game_level = models.ForeignKey(GameLevel)
    plan = models.ForeignKey(Plan, blank=True, null=True)
    play_time = models.DateTimeField(default=datetime.now, blank=True, null=True)
    extra_data = models.TextField(blank=True, null=True)
    score = models.FloatField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '[%s] played by [%s] with plan [%s] created [%s]' % (self.game_level, self.player, self.plan, naturaltime(self.created))

# return '%s for [%s] created [%s]' % (self.game_play.game_level, self.game_play.player, naturaltime(self.created))

class PlayerGameDataRecord(models.Model):
    game_play = models.ForeignKey(PlayerGamePlay)
    data_type = models.CharField(max_length=100, blank=True, null=True)
    data_schema = models.CharField(max_length=100, blank=True, null=True)
    data = models.TextField(blank=True, null=True)
    is_checked = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '[%s] for [%s] created [%s]' % (self.game_play.game_level, self.game_play.player, naturaltime(self.created))


class UserGameFeedback(models.Model):
    player = models.ForeignKey(Player)
    game = models.ForeignKey(Game)
    rank = models.IntegerField()
    comments = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s 's rank on %s is %d" % (self.user, self.game, self.rank)


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='userprofile')
    user_type = models.CharField(max_length=30, choices=USERROLE_CHOICES, default='player')

    def __unicode__(self):
        return "%s %s's profile" % (self.user_type, self.user)


@receiver(post_save, sender=get_user_model())
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def create_userprofile(sender, instance, created, **kwargs):
    if created:
        userprofile = UserProfile.objects.create(user=instance)
        # if userprofile.user_type == 'player':
        #     Player.objects.create(user=instance)

models.signals.post_save.connect(create_userprofile, sender=User)


###########################################################################################

# blogpost type app models
class Post(models.Model):
    author = models.ForeignKey(User, related_name='posts')
    title = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True)


class Photo(models.Model):
    post = models.ForeignKey(Post, related_name='photos')
    image = models.ImageField(upload_to="%Y/%m/%d")


# profile type app models
class Profile(models.Model):

    user = models.OneToOneField(User, related_name='profile')
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.user.username


class ProfilePhoto(models.Model):

    profile = models.ForeignKey(Profile, related_name='photos')
    title = models.CharField(max_length=1000, null=True, blank=True)
    image = models.ImageField(upload_to='images/%Y/%m/%d', null=True, blank=True)

    def __unicode__(self):
        return self.title or 'noname'

    def get_absolute_url(self):
        return self.image.url if self.image else ''


def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance, name=instance.username)


models.signals.post_save.connect(create_profile, sender=User)
