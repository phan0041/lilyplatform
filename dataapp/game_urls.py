from django.conf.urls import patterns, url, include

from .game_api import UserDetail
from .game_api import GameDetail

from .game_api import StaffDetail
from .game_api import StaffPlayerList

from .game_api import PlayerList, PlayerDetail
from .game_api import PlayerGameList
from .game_api import PlayerGamePlanList
from .game_api import PlayerGameFeedbackList

from .game_api import PlayList, PlayDetail
from .game_api import DataRecordList, DataRecordDetail
from .game_api import FeedbackList
from .game_api import PlanList, PlanDetail

from .game_api import PlayerGamelevelPlayList, PlayerGamelevelPlayStats
from .game_api import PlayDatarecordList, PlayDatarecordStats


user_urls = patterns('',
    url(r'^/(?P<username>[0-9a-zA-Z_-]+)/$', UserDetail.as_view(), name='user-detail'),
)

staff_urls = patterns('',
    url(r'^/(?P<pk>\d+)/players$', StaffPlayerList.as_view(), name='staffplayer-list'),
    url(r'^/(?P<pk>\d+)$', StaffDetail.as_view(), name='staff-detail')
)

player_urls = patterns('',
    url(r'^/(?P<pk>\d+)/games/(?P<game_uuid>[0-9a-zA-Z_-]+)/plans$', PlayerGamePlanList.as_view(), name='playergameplan-list'),
    url(r'^/(?P<pk>\d+)/games/(?P<game_uuid>[0-9a-zA-Z_-]+)/feedbacks$', PlayerGameFeedbackList.as_view(), name='playergamefeedback-list'),
    url(r'^/(?P<pk>\d+)/game_levels/(?P<gamelevel_uuid>[0-9a-zA-Z_-]+)/plays$', PlayerGamelevelPlayList.as_view(), name='playergamelevelplay-list'),
    url(r'^/(?P<pk>\d+)/game_levels/(?P<gamelevel_uuid>[0-9a-zA-Z_-]+)/data$', PlayerGamelevelPlayStats.as_view(), name='playergamelevelplay-stats'),
    url(r'^/(?P<pk>\d+)/games$', PlayerGameList.as_view(), name='playergame-list'), # to be implemented
    url(r'^/(?P<pk>\d+)$', PlayerDetail.as_view(), name='player-detail'),
    url(r'^$', PlayerList.as_view(), name='player-list')
)

game_urls = patterns('',
    url(r'^/(?P<uuid>[0-9a-zA-Z_-]+)$', GameDetail.as_view(), name='game-detail'),
)

play_urls = patterns('',
    url(r'^/(?P<pk>\d+)/datarecords$', PlayDatarecordList.as_view(), name='playdatarecord-list'),
    url(r'^/(?P<pk>\d+)/data$', PlayDatarecordStats.as_view(), name='playdatarecord-stats'),
    url(r'^/(?P<pk>\d+)$', PlayDetail.as_view(), name='play-detail'),
    url(r'^$', PlayList.as_view(), name='play-list')
)

datarecord_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', DataRecordDetail.as_view(), name='datarecord-detail'),
    url(r'^$', DataRecordList.as_view(), name='datarecord-list')
)

feedback_urls = patterns('',
    url(r'^$', FeedbackList.as_view(), name='feedback-list')
)

plan_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', PlanDetail.as_view(), name='plan-detail'),
    url(r'^$', PlanList.as_view(), name='plan-list')
)

gameplatform_urls = patterns('',
    url(r'^users', include(user_urls)),
    url(r'^staffs', include(staff_urls)),
    url(r'^players', include(player_urls)),
    url(r'^games', include(game_urls)),
    url(r'^plays', include(play_urls)),
    url(r'^datarecords', include(datarecord_urls)),
    url(r'^feedbacks', include(feedback_urls)),
    url(r'^plans', include(plan_urls)),
)
