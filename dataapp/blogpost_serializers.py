from rest_framework import generics, permissions
from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Post, Photo


class UserSerializer(serializers.ModelSerializer):
    posts = serializers.HyperlinkedIdentityField('posts', view_name='userpost-list', lookup_field='username')

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'posts', )


class PostSerializer(serializers.ModelSerializer):
    # makes author readily accessible rather than requiring extra API requests.
    # The alternative hyperlink is listed with a comment for comparison.
    # The power of serializers is that you can extend them to create a derivative version, that uses hyperlinks instead of nested records (say, for the case of listing posts by a specific users' feed).

    author = UserSerializer(required=False)
    # author = serializers.HyperlinkedRelatedField(view_name='user-detail', lookup_field='username')

    photos = serializers.HyperlinkedIdentityField('photos', view_name='postphoto-list')

    def get_validation_exclusions(self, instance=None):
        # Need to exclude `author` since we'll add that later based off the request
        exclusions = super(PostSerializer, self).get_validation_exclusions(instance)
        return exclusions + ['author']

    class Meta:
        model = Post


class PhotoSerializer(serializers.ModelSerializer):
    image = serializers.Field('image.url')

    class Meta:
        model = Photo
