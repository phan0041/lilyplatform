from django.contrib.auth.models import User

from rest_framework import generics, permissions
from rest_framework import status
from rest_framework.response import Response

from .game_serializers import UserDetailsSerializer, PlayerSerializer
from .game_serializers import GameSerializer
from .game_serializers import PlaySerializer, PlayerFullSerializer
from .game_serializers import DataRecordSerializer, FeedbackSerializer
from .game_serializers import PlanSerializer

from .game_permissions import CanAccessUserPermission, CanAccessPlayerPermission
from .game_permissions import CanAccessPlayPermission, CanAccessDataRecordPermission
from .game_permissions import CanAccessPlanPermission

from .models import Game, Player, Staff, Plan
from .models import PlayerGamePlay, PlayerGameDataRecord
from .models import UserGameFeedback
from .models import UserProfile

from dataapp.utils.utils import all_session_visualize, single_session_visualize


class UserDetail(generics.RetrieveAPIView):
    model = User
    serializer_class = UserDetailsSerializer
    lookup_field = 'username'
    permission_classes = [
        CanAccessUserPermission
    ]


class PlayerList(generics.ListCreateAPIView):
    model = Player
    serializer_class = PlayerSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def pre_save(self, obj):
        # create user info
        if 'user' in self.request.DATA:
            user_data = self.request.DATA['user']
            user = User.objects.create_user(user_data['username'], email=None, password='123456')
            if 'first_name' in user_data:
                user.first_name = user_data['first_name']
            if 'last_name' in user_data:
                user.last_name = user_data['last_name']
            user.save()

            userprofile = UserProfile.objects.get(user=user)
            userprofile.user_type = 'player'
            userprofile.save()

            obj.user = user
        return super(PlayerList, self).pre_save(obj)

    def post_save(self, obj, created=False):
        if self.request.user.userprofile.user_type == 'staff':
            staff = self.request.user.staff_profile
            obj.doctors.add(staff)
        return super(PlayerList, self).post_save(obj)


class PlayerDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Player
    serializer_class = PlayerSerializer
    permission_classes = [
        CanAccessPlayerPermission
    ]

    def pre_save(self, obj):
        # update user info
        if 'user' in self.request.DATA:
            user_data = self.request.DATA['user']
            User.objects.filter(username=user_data['username']).update(**user_data)


class StaffPlayerList(generics.ListCreateAPIView):
    model = Player
    serializer_class = PlayerFullSerializer
    permission_classes = [
        CanAccessPlayerPermission
    ]

    def get_queryset(self):
        queryset = super(StaffPlayerList, self).get_queryset()
        return queryset.filter(doctors__pk=self.kwargs.get('pk'))


class StaffDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Staff


class GameDetail(generics.RetrieveAPIView):
    model = Game
    lookup_field = 'uuid'
    serializer_class = GameSerializer


class PlayList(generics.ListCreateAPIView):
    model = PlayerGamePlay
    serializer_class = PlaySerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def pre_save(self, obj):
        obj.player = self.request.user.player_profile


class DataRecordList(generics.ListCreateAPIView):
    model = PlayerGameDataRecord
    serializer_class = DataRecordSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]


class PlayDatarecordList(generics.ListAPIView):
    model = PlayerGameDataRecord
    serializer_class = DataRecordSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get_queryset(self):
        queryset = super(PlayDatarecordList, self).get_queryset()
        return queryset.filter(game_play__pk=self.kwargs.get('pk'))


class PlayDatarecordStats(PlayDatarecordList):
    def list(self, request, pk=None):  # pk is the play id
        # query_params = self.request.QUERY_PARAMS.dict()
        vis_data = single_session_visualize(pk)
        return Response(vis_data)


class FeedbackList(generics.ListCreateAPIView):
    model = UserGameFeedback
    serializer_class = FeedbackSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def pre_save(self, obj):
        obj.player = self.request.user.player_profile


class PlanList(generics.ListCreateAPIView):
    model = Plan
    serializer_class = PlanSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]


class PlanDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Plan
    serializer_class = PlanSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]


class PlayerGamePlanList(generics.ListAPIView):
    model = Plan
    permission_classes = (CanAccessPlanPermission, )

    def get_queryset(self):
        queryset = super(PlayerGamePlanList, self).get_queryset()
        return queryset.filter(player__pk=self.kwargs.get('pk')).filter(game__uuid=self.kwargs.get('game_uuid'))


class PlayerGameFeedbackList(generics.ListAPIView):
    model = UserGameFeedback
    serializer_class = FeedbackSerializer

    def get_queryset(self):
        queryset = super(PlayerGameFeedbackList, self).get_queryset()
        return queryset.filter(player__pk=self.kwargs.get('pk')).filter(game__uuid=self.kwargs.get('game_uuid'))


class PlayerGamelevelPlayList(generics.ListAPIView):
    model = PlayerGamePlay
    permission_classes = [
        CanAccessPlayPermission
    ]

    def get_queryset(self):
        queryset = super(PlayerGamelevelPlayList, self).get_queryset()
        return queryset.filter(player__pk=self.kwargs.get('pk')).filter(game_level__uuid=self.kwargs.get('gamelevel_uuid'))


class PlayerGamelevelPlayStats(PlayerGamelevelPlayList):
    def list(self, request, gamelevel_uuid=None, pk=None):
        # query_params = self.request.QUERY_PARAMS.dict()
        vis_data = all_session_visualize(pk, gamelevel_uuid)
        return Response(vis_data)


# not implemented yet
class PlayDetail(generics.RetrieveAPIView): # to be moved to /players/1/games/2/plays
    model = PlayerGamePlay
    serializer_class = PlaySerializer
    permission_classes = [
        CanAccessPlayPermission
    ]


class DataRecordDetail(generics.RetrieveAPIView):  # to be moved to /players/1/games/2/plays/2/datarecords
    model = PlayerGameDataRecord
    # serializer_class = PhotoSerializer
    permission_classes = [
        CanAccessDataRecordPermission
    ]


class PlayerGameList(generics.ListAPIView):
    pass
