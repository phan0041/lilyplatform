# -*- coding: utf-8 -*-
from datetime import datetime
import urllib2
import json
import os
from django.contrib.auth.models import User
from django.utils.encoding import smart_str, smart_unicode
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from django.core.mail import EmailMessage

# from dateutil import parser
from dataapp.models import *

class Command(BaseCommand):
    def create_user(self):
        for i in range(1, 401):
            username = 'SGERC_'+ str(i)
            email = username + '@gmail.com'
            password = 'psw'+ str(i)
            user = User.objects.create_user(username=username, email=email, password=password)
            user.save()

            player = Player(user=user, role='nonpatient')
            player.save()

    def handle(self, *args, **options):
        self.create_user()
