{{ project_name }}
========

{{ project_name }} will solve your problem of where to start with documentation,
by providing a basic explanation of how to do it easily.

Look how easy it is to use:

    import project
    # Get your stuff done
    project.do_stuff()

Features
--------

- Be awesome
- Make things faster

Installation
------------

Install {{ project_name }} by running:

    install project

Contribute
----------

- Issue Tracker: github.com/{{ project_name }}/{{ project_name }}/issues
- Source Code: github.com/{{ project_name }}/{{ project_name }}

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: project@google-groups.com

License
-------

The project is licensed under the BSD license.
