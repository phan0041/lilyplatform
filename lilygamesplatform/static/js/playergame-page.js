var playergameApp = angular.module('playergameApp', []);


playergameApp.config(['$httpProvider', '$interpolateProvider',
    function($httpProvider, $interpolateProvider) {
    /* for compatibility with django teplate engine */
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    /* csrf */
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}]);

playergameApp.controller('playergameCtrl', function ($scope, $element, $http) {
    $scope.data = {success: false}
    $scope.plan_list = []
    $scope.game = null
    $scope.selected_level = null
    $scope.selected_level_plays = null
    $scope.selected_play = null
    $scope.selected_play_records = null

    $http({method: 'GET', url: '/api/players/' + vars.player_id + '/games/' + vars.game_uuid + '/plans'}).
    success(function(data, status, headers, config) {
        $scope.plan_list = data;
    })

    $http({method: 'GET', url: '/api/games/' + vars.game_uuid}).
    success(function(data, status, headers, config) {
        $scope.game = data;
    })

    $scope.switchGameLevel = function() {
        console.log($scope.selected_level)

        // load plays /player/xxx/game_levels/xxx/plays
        $http({method: 'GET', url: '/api/players/' + vars.player_id + '/game_levels/' + $scope.selected_level.uuid + '/plays'}).
        success(function(data, status, headers, config) {
            $scope.selected_level_plays = data;
        })
    }

    $scope.switchPlay = function() {
        console.log($scope.selected_play)

        // load datarecords /api/plays/9/datarecords
        $http({method: 'GET', url: '/api/plays/' + $scope.selected_play.id + '/datarecords'}).
        success(function(data, status, headers, config) {
            $scope.selected_play_records = data;
        })
    }

    $scope.updatePlan = function(plan, index) {
        $scope.editingplan = plan;
        $('#plan-edit-modal').modal('toggle')
    }

    $scope.addPlan = function() {
        $scope.editingplan = null;
        $('#plan-edit-modal').modal('toggle')
    }

    $scope.submitPlanInfo = function() {
        console.log('plan data: ', $scope.editingplan)

        if ($scope.editingplan.id) {
            var method = 'PATCH';
            var api_endpoint = '/api/plans/' + $scope.editingplan.id
        } else {
            var method = 'POST';
            var api_endpoint = '/api/plans'
        }

        if (! $scope.editingplan.player) {
            $scope.editingplan.player = vars.player_id
        }
        if (! $scope.editingplan.game) {
            $scope.editingplan.game = vars.game_id
        }
        if (! $scope.editingplan.staff) {
            $scope.editingplan.staff = vars.staff_id
        }

        console.log('plan data: ', $scope.editingplan)

        $http({method: method, url: api_endpoint, data: $scope.editingplan}).
        success(function(data, status, headers, config) {
            console.log('offer return data', data)
            $scope.data['success'] = true;
            if (method == 'POST') {
                $scope.plan_list.push($scope.editingplan)
            }
        }).
        error(function(data, status, headers, config) {
            $scope.data['success'] = false;
        });
        $('#plan-edit-modal').modal('toggle')
    }
});
