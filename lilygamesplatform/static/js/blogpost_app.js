var blogApp = angular.module('blogApp', ['ngResource']);


blogApp.config(['$httpProvider', '$interpolateProvider',
    function($httpProvider, $interpolateProvider) {
    /* for compatibility with django teplate engine */
    // $interpolateProvider.startSymbol('{$');
    // $interpolateProvider.endSymbol('$}');
    /* csrf */
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}]);


blogApp.factory('User', ['$resource', function($resource) {
        return $resource('/dataapp/api/blogpost/users/:username')
    }]);


blogApp.factory('Post', ['$resource', function($resource) {
        return $resource('/dataapp/api/blogpost/posts/:id')
    }]);


blogApp.factory('Photo', ['$resource', function($resource) {
        return $resource('/dataapp/api/blogpost/photos/:id')
    }]);


blogApp.factory('UserPost', ['$resource', function($resource) {
        return $resource('/dataapp/api/blogpost/users/:username/posts:id')
    }]);


blogApp.factory('PostPhoto', ['$resource', function($resource) {
        return $resource('/dataapp/api/blogpost/posts/:post_id/photos/:id')
    }]);


blogApp.controller('blogCtrl', function ($scope, AuthUser, Post, PostPhoto) {
    $scope.posts = Post.query()
    $scope.photos = {}
    $scope.newPost = new Post()

    $scope.posts.$promise.then(function(results) {
        var log = [];
        angular.forEach(results, function(post) {
          $scope.photos[post.id] = PostPhoto.query({'post_id': post.id})
        }, log);
    })

    $scope.save = function() {// create new post
        $scope.newPost.$save().then (function(result) { // success
            console.log('data: ', result)
            $scope.posts.push(result)

        }).then(function(result) {
            $scope.newPost = new Post()
        }).then(function(){
            $scope.errors = null
        }, function(result) { // error
            console.log('error: ', result.data)
            $scope.errors = result.data
        })
    }

    $scope.canDelete = function(post) {
        return post.author.username == AuthUser.username
    }

    $scope.delete = function(post) {
        post.$delete(params = {id: post.id}).then (function(){
            idx = $scope.posts.indexOf(post)
            $scope.posts.splice(idx, 1)
        })
    }
});