var staffdashboardApp = angular.module('staffdashboardApp', []);


staffdashboardApp.config(['$httpProvider', '$interpolateProvider',
    function($httpProvider, $interpolateProvider) {
    /* for compatibility with django teplate engine */
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    /* csrf */
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    // $httpProvider.defaults.headers.common['Authorization'] = 'Token 2c83e6ad86f51b0104deb2c10a28b1c8b401ee33'; // doctor 1's token
    // $httpProvider.defaults.headers.common["Content-Type"] = 'application/json'
}]);

staffdashboardApp.controller('staffdashboardCtrl', function ($scope, $element, $http) {
    $scope.data = {success: false}
    $scope.player_list = []
    $scope.current_showing_block = 'no_selection'  // block to show on right side (no_selection, select_player, select_game)
    $scope.selectedplayer = null
    $scope.selectedgame_uuid = null
    $scope.selectedgame_id = null

    $scope.selectedplayer_plan_list = []
    $scope.selectedgame = null
    $scope.selected_level = null
    $scope.selected_level_plays = null
    $scope.selected_play = null
    $scope.selected_play_records = null

    $http({method: 'GET', url: '/api/staffs/1/players'}).
    success(function(data, status, headers, config) {
        $scope.player_list = data;
    })

    $scope.updatePlayer = function(player, index) {
        $scope.editingplayer = player;
        $('#player-edit-modal').modal('toggle')
    }

    $scope.addPlayer = function() {
        $scope.editingplayer = null;
        $('#player-edit-modal').modal('toggle')
    }

    $scope.submitPlayerInfo = function() {
        console.log('player data: ', $scope.editingplayer)

        if ($scope.editingplayer.id) {
            var method = 'PATCH';
            var api_endpoint = '/api/players/' + $scope.editingplayer.id
        } else {
            var method = 'POST';
            var api_endpoint = '/api/players'
        }

        $http({method: method, url: api_endpoint, data: $scope.editingplayer}).
        success(function(data, status, headers, config) {
            console.log('offer return data', data)
            $scope.data['success'] = true;
            if (method == 'POST') {
                $scope.editingplayer.id = data['id']
                $scope.player_list.push($scope.editingplayer)
            }
        }).
        error(function(data, status, headers, config) {
            $scope.data['success'] = false;
        });
        $('#player-edit-modal').modal('toggle')
    }



    $scope.selectPlayer = function(player, index) {
        $scope.selectedplayer = player
        $scope.current_showing_block = 'select_player'
        $scope.selectedgame_uuid = null
        $scope.selectedgame_id = null
        $scope.selected_level = null
        $scope.selected_play = null
    }

    $scope.selectGame = function(game_uuid, game_id) {
        $scope.selected_level = null
        $scope.selected_play = null
        $scope.selectedgame_uuid = game_uuid;
        $scope.selectedgame_id = game_id;

        $scope.current_showing_block = 'select_game'

        $http({method: 'GET', url: '/api/players/' + $scope.selectedplayer.id + '/games/' + game_uuid + '/plans'}).
        success(function(data, status, headers, config) {
            console.log('got plans')
            $scope.selectedplayer_plan_list = data;
        })

        $http({method: 'GET', url: '/api/games/' + game_uuid}).
        success(function(data, status, headers, config) {
            console.log('got game details')
            $scope.selectedgame = data;
        })
    }

    $scope.switchGameLevel = function() {
        $scope.selected_play = null
        // load plays /player/xxx/game_levels/xxx/plays
        $http({method: 'GET', url: '/api/players/' + $scope.selectedplayer.id + '/game_levels/' + $scope.selected_level.uuid + '/plays'}).
        success(function(data, status, headers, config) {
            console.log('switchGameLevel(): Loaded task raw data.');
            console.log(data);
            $scope.selected_level_plays = data;
        })

        $http({method: 'GET', url: '/api/players/' + $scope.selectedplayer.id + '/game_levels/' + $scope.selected_level.uuid + '/data'}).
        success(function(data, status, headers, config) {
            console.log('switchGameLevel(): Loaded processed data');
            console.log(data);
            task_vis($scope.selected_level, data)
        })
    }

    $scope.switchPlay = function() {
        // load datarecords /api/plays/9/datarecords
        $http({method: 'GET', url: '/api/plays/' + $scope.selected_play.id + '/datarecords'}).
        success(function(data, status, headers, config) {
            console.log('switchPlay(): loaded play raw data');
            console.log(data);
            $scope.selected_play_records = data;
        })

        $http({method: 'GET', url: '/api/plays/' + $scope.selected_play.id + '/data'}).
        success(function(data, status, headers, config) {
            // load visualization functions
            console.log('switchPlay(): loaded processed data');
            console.log(data);
            play_vis($scope.selected_level, data)
        })
    }

    $scope.updatePlan = function(plan, index) {
        $scope.editingplan = plan;
        $scope.editingplan.json = angular.fromJson(plan.plan_data)
        $('#plan-edit-modal').modal('toggle')
    }

    $scope.addPlan = function() {
        $scope.editingplan = null;
        $('#plan-edit-modal').modal('toggle')
    }

    $scope.submitPlanInfo = function() {
        console.log('raw plan data: ', $scope.editingplan)
        $scope.editingplan.plan_data = angular.toJson($scope.editingplan.json)
        console.log('jsonified plan data: ', $scope.editingplan)

        if ($scope.editingplan.id) {
            var method = 'PATCH';
            var api_endpoint = '/api/plans/' + $scope.editingplan.id
        } else {
            var method = 'POST';
            var api_endpoint = '/api/plans'
        }

        if (! $scope.editingplan.player) {
            $scope.editingplan.player = $scope.selectedplayer.id
        }
        if (! $scope.editingplan.game) {
            $scope.editingplan.game = $scope.selectedgame_id
        }
        if (! $scope.editingplan.staff) {
            $scope.editingplan.staff = vars.staff_id
        }

        console.log('plan data: ', $scope.editingplan)

        $http({method: method, url: api_endpoint, data: $scope.editingplan}).
        success(function(data, status, headers, config) {
            console.log('offer return data', data)
            $scope.data['success'] = true;
            if (method == 'POST') {
                $scope.selectedplayer_plan_list.push($scope.editingplan)
            }
        }).
        error(function(data, status, headers, config) {
            $scope.data['success'] = false;
        });
        $('#plan-edit-modal').modal('toggle')
    }

});
