var linechart = function(selector, data, xlabel, ylabel) {
  var chart = nv.models.lineChart()
    .useInteractiveGuideline(true)
    ;

  chart.xAxis
    .axisLabel(xlabel)
    .tickFormat(d3.format(',r'))
    ;

  chart.yAxis
    .axisLabel(ylabel)
    .tickFormat(d3.format('.02f'))
    ;

  d3.select(selector)
    .datum(data)
    .transition().duration(500)
    .call(chart)
    ;

  nv.utils.windowResize(chart.update);

  return chart;
}

var lv_task_vis= function(data){
  nv.addGraph(linechart('#chart svg', data, 'Time (ms)', 'Voltage (v)'));
}
