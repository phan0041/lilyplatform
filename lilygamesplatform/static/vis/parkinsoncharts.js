
var parkinson_task_vis= function(data){
  //nv.addGraph(linechart('#chart svg', data, 'Time (ms)', 'Voltage (v)'));
  selectors = ['#chart1 svg','#chart2 svg'];
  document.getElementById('task_data').innerHTML = data;
  chart_allsession(selectors,data);
}

var parkinson_play_vis= function(data){
    selectors = ['#overall-chart svg', '#memoryblock svg'];
    document.getElementById('play_data').innerHTML = data;
    chart_play(selectors, data);
}

function chart_play(selectors,data){

    //dummy
    var pm1_val = 0
    var pm1_text = "PM1: " + pm1_val;
    var pm2_val = 0
    var pm2_text = "PM2: " + pm2_val;
    var pm3_val = 76
    var pm3_text = "PM3: " + pm3_val;
    var scores = [50,50,25]
    //enddummy

    var data = [
      {
        "key": "session performance",
        "color": "#d62728",
        "values": [
          {
            "label" : "Absorption score" ,
            "value" : scores[0]
          } ,
          {
            "label" : "Speed score" ,
            "value" : scores[1]
          } ,
          {
            "label" : "Memory score" ,
            "value" : scores[2]
          }
        ]
      }
    ]

    nv.addGraph(function() {
      var chart = nv.models.multiBarHorizontalChart()
          .x(function(d) { return d.label })
          .y(function(d) { return d.value })
          .margin({top: 100, right: 20, bottom: 50, left: 125})
          .height(240)
          .showValues(true)
          .tooltips(false)
          .showLegend(false)
          .showControls(false);

      chart.yAxis
          .tickFormat(d3.format('d'));

      chart.forceY([0,100])

      d3.select(selectors[0])
          .datum(data)
        .transition().duration(500)
          .call(chart);

      nv.utils.windowResize(chart.update);

      return chart;
    });



    var pi = Math.PI;
    var vis = d3.select(selectors[1]);

    var dy = -5
    var x = 60
    var opacity = 0.8
    var colors = ["#6baed6", "#fd8d3c", "#74c476", "#ff8c00"]
    var background = "#D3D3D3";

    var arc = d3.svg.arc()
        .innerRadius(90)
        .outerRadius(120)
        .startAngle(0);


    vis.append("path") // background arc
        .attr("class", "circle background")
        .attr("transform", "translate(140,170)")
        .attr("id", "path1")
        .attr("d", arc.startAngle(0), arc.endAngle((1/3) * 2*pi-0.1))
        .attr("fill", background)
        .attr("opacity", opacity);

    vis.append("path")
        .attr("class", "circle progress")
        .attr("transform", "translate(140,170)")
        .attr("d", arc.startAngle(0), arc.endAngle(((1/3) * 2*pi-0.1)*pm1_val*0.01))
        .attr("fill", colors[0])
        .attr("data-legend","PM1: Time-based (after 30 seconds)")
        .attr("data-legend-pos", 1);;

    var text = vis.append("text")
        .attr("x", x)
        .attr("dy", dy);
    text.append("textPath")
        .attr("xlink:href","#path1")
        .text(pm1_text);


    vis.append("path") // background arc
        .attr("class", "circle background")
        .attr("transform", "translate(140,170)")
        .attr("id", "path2")
        .attr("d", arc.startAngle((1/3) * 2*pi), arc.endAngle((2/3) * 2*pi-0.1))
        .attr("fill", background)
        .attr("opacity", opacity);

    vis.append("path")
        .attr("class", "circle progress")
        .attr("transform", "translate(140,170)")
        .attr("d", arc.startAngle((1/3) * 2*pi), arc.endAngle((1/3) * 2*pi + ((1/3) * 2*pi-0.1)*pm2_val*0.01))
        .attr("fill", colors[1])
        .attr("data-legend","PM2: Time-based (at a particular time)")
        .attr("data-legend-pos", "2");

    var text = vis.append("text")
        .attr("x", x)
        .attr("dy", dy);
    text.append("textPath")
        .attr("xlink:href","#path2")
        .text(pm2_text);


    vis.append("path") // background arc
        .attr("class", "circle background")
        .attr("transform", "translate(140,170)")
        .attr("id", "path3")
        .attr("d", arc.startAngle((2/3) * 2*pi), arc.endAngle((3/3) * 2*pi-0.1))
        .attr("fill", background)
        .attr("opacity", opacity);

    vis.append("path")
        .attr("class", "circle progress")
        .attr("transform", "translate(140,170)")
        // .attr("d", arc.startAngle((2/3) * 2*pi), arc.endAngle((9/10) * 2*pi)) //pm3_val
        .attr("d", arc.startAngle((2/3) * 2*pi), arc.endAngle((2/3) * 2*pi + ((1/3) * 2*pi-0.1)*pm3_val*0.01))
        .attr("fill", colors[2])
        .attr("data-legend","PM3: Event-based (at the appearance of the boat)")
        .attr("data-legend-pos", "3");;

    var text = vis.append("text")
        .attr("x", x)
        .attr("dy", dy);
    text.append("textPath")
        .attr("xlink:href","#path3")
        .text(pm3_text);

    /*
    var legend = vis.append("svg")
       .attr("class", "legend")
       .attr("width", 180)
       .attr("height", 180)
       .selectAll("g")
       .data(color.domain().slice().reverse())
       .enter().append("g")
       .attr("transform", function(d, i) { return "translate(100," + i * 20 + ")"; })
    */


    legend = vis.append("g")
        .attr("class","legend")
        .attr("transform","translate(250,30)")
        .style("font-size","12px")
        .call(d3.legend)
}

function chart_alluser(selectors, data){
    data = null;
    // transform data here
    // var time_scores = {{user_avg_scores.user_avg_time_scores}};
    // var collect_scores = {{user_avg_scores.user_avg_collect_scores}};
    // var memory_scores = {{user_avg_scores.user_avg_memory_scores}};

    // var overall_scores = {{user_avg_scores.user_overall_scores}};
    
    //dummy data
    var time_scores = [40, 59, 0];
    var collect_scores = [50, 50, 0];
    var memory_scores = [50, 50, 0];

    var overall_scores = [46.666666666666664, 53, 0];
    //end of dummy data

    var memory_score_series = [];
    var time_score_series = [];
    var collect_score_series = [];
    
    for (var i = 0; i < time_scores.length; i++) {
      memory_score_series.push({x: i+1, y: memory_scores[i]});
      time_score_series.push({x: i+1, y: time_scores[i]});
      collect_score_series.push({x: i+1, y: collect_scores[i]});
    }

    var build_chart = function(values, div, height) {
      // A formatter for counts.
      var formatCount = d3.format(",.0f");

      var margin = {top: 10, right: 30, bottom: 30, left: 30},
          width = 450 - margin.left - margin.right,
          height = height - margin.top - margin.bottom;

      var x = d3.scale.linear()
          .domain([0, 100])
          .range([0, width]);

      // Generate a histogram using twenty uniformly-spaced bins.
      var data = d3.layout.histogram()
          .bins(x.ticks(20))
          (values);

      var y = d3.scale.linear()
          .domain([0, d3.max(data, function(d) { return d.y; })])
          .range([height, 0]);

      var xAxis = d3.svg.axis()
          .scale(x)
          .orient("bottom");

      //var svg = d3.select(div).append("svg")

      var svg = d3.select(div).append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      var bar = svg.selectAll(".bar")
          .data(data)
        .enter().append("g")
          .attr("class", "bar")
          .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

      bar.append("rect")
          .attr("x", 1)
          .attr("width", x(data[0].dx) - 1)
          .attr("height", function(d) { return height - y(d.y); });

      bar.append("text")
          .attr("dy", ".75em")
          .attr("y", 6)
          .attr("x", x(data[0].dx) / 2)
          .attr("text-anchor", "middle")
          .text(function(d) { return formatCount(d.y); });

      svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis);
    }

    //build_chart(overall_scores, '#chart1', 480);

    // build_chart(memory_scores, '#chart2', 120);
    // build_chart(time_scores, '#chart3', 120)
    // build_chart(collect_scores, '#chart4', 120)

    // scatterPlot3d( d3.select('#chart1'), memory_score_series, time_score_series, collect_score_series);

    build_chart(memory_scores, selectors[1], 120);
    build_chart(time_scores, selectors[2], 120)
    build_chart(collect_scores, selectors[3], 120)

    scatterPlot3d( d3.select(selectors[0]), memory_score_series, time_score_series, collect_score_series);
}

function chart_allsession(selectors,data){

  // data retriving
  // var time_scores = {{user_scores.user_time_scores}};
  // var collect_scores = {{user_scores.user_collect_scores}};
  // var memory_scores = {{user_scores.user_memory_scores}};
  // var pm_flip_memory_scores = {{user_scores.pm_flip_memory_scores}};
  // var pm_fairy_memory_scores = {{user_scores.pm_fairy_memory_scores}};
  // var pm_fisherman_memory_scores = {{user_scores.pm_fisherman_memory_scores}};

  //dummy data
  var time_scores = [30, 50]
  var collect_scores = [50, 50]
  var memory_scores = [76, 25]
  var pm_flip_memory_scores = [76, 0]
  var pm_fairy_memory_scores = [76, 0]
  var pm_fisherman_memory_scores = [76, 76]
  //enddummy

  var memory_score_series = [];
  var time_score_series = [];
  var negative_time_score_series = [];
  var collect_score_series = [];
  var negative_collect_score_series = [];

  var pm_flip_memory_series = []
  var pm_fairy_memory_series = []
  var pm_fisherman_memory_series = []

  for (var i = 0; i < pm_flip_memory_scores.length; i++) {
    pm_flip_memory_series.push({x: i+1, y: pm_flip_memory_scores[i]});
    pm_fairy_memory_series.push({x: i+1, y: pm_fairy_memory_scores[i]});
    pm_fisherman_memory_series.push({x: i+1, y: pm_fisherman_memory_scores[i]});
  }

  for (var i = 0; i < time_scores.length; i++) {
    memory_score_series.push({x: i+1, y: memory_scores[i]});
    time_score_series.push({x: i+1, y: time_scores[i]});
    negative_time_score_series.push({x: i+1, y: -time_scores[i]});
    collect_score_series.push({x: i+1, y: collect_scores[i]});
    negative_collect_score_series.push({x: i+1, y: -collect_scores[i]});
  }

  // ----------------------------------
  var chart1_data = function() {
    return [
      {
        values: memory_score_series,
        key: 'Prospective Memory',
        color: '#1F77B4'
      },
      {
        values: time_score_series,
        key: 'Speed',
        color: '#ff7f0e'
      },
      {
        values: collect_score_series,
        key: 'Absorption',
        color: '#2ca02c'
      }
    ];
  }


  nv.addGraph(function() {
    var chart = nv.models.lineChart()
      .useInteractiveGuideline(true)
      .forceY([0,100])
      ;

    chart.xAxis
      .axisLabel('Session Index')
      .tickFormat(d3.format(',d'))
      ;

    chart.yAxis
      .axisLabel('Voltage (v)')
      .tickFormat(d3.format('d'))
      ;

    d3.select(selectors[0])
      .datum(chart1_data())
      .transition().duration(500)
      .call(chart)
      ;

    nv.utils.windowResize(chart.update);

    return chart;
  });

  // ----------------------------------

  var layers = [
    {
      "key": "Flip",
      "values": pm_flip_memory_series,
      "color": "#d62728"
    },
    // {
    //   "key": "Prospective Memory",
    //   "values": leftover_memory_score_series,
    //   "color": "#D3D3D3"
    // },
    {
      "key": "Fairy",
      "values": pm_fairy_memory_series,
      "color": "#9467bd"
    },
    // {
    //   "key": "Speed",
    //   "values": leftover_time_score_series,
    //   "color": "#D3D3D3"
    // },
    {
      "key": "Fisherman",
      "values": pm_fisherman_memory_series,
      "color": "#8c564b"
    },
    // {
    //   "key": "Absorption",
    //   "values": leftover_collect_score_series,
    //   "color": "#D3D3D3"
    // },
  ];


  nv.addGraph(function() {
    var chart = nv.models.lineChart()
      .forceY([0,100])
      .useInteractiveGuideline(true)
      ;

    chart.xAxis
      .axisLabel('Session Index')
      .tickFormat(d3.format(',d'))
      ;

    chart.yAxis
      .axisLabel('Voltage (v)')
      .tickFormat(d3.format('d'))
      ;

    d3.select(selectors[1])
      .datum(layers)
      .transition().duration(500)
      .call(chart)
      ;

    nv.utils.windowResize(chart.update);

    return chart;
  });
}

function chart_singlesession(){

}


// add graph
// paste all old visualization code here. 
// pass processed data into here

