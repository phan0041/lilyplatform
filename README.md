django-project-template
=====================

#### Features ####
* [Integrate django-rest-auth](https://github.com/Tivix/django-rest-auth)
* [Integrate multiple bootstrap-based themes. E.g., FLAT-UI and paper](http://designmodo.github.io/Flat-UI/)

#### Usage ####

    django-admin.py startproject --template=https://github.com/chrishan/django-project-template/zipball/master <project_name>

#### Getting Started ####

    pip install virtualenv
    virtualenv mysiteenv
    source mysiteenv/bin/activate
    pip install Django==1.6.2
    django-admin.py startproject --template=https://github.com/chrishan/django-project-template/zipball/master mysite
    cd mysite
    (for Windows) easy_install pycrypto-xxx.exe
    (for WIndows) easy_install pywin32-xxx.exe
    pip install -r requirements.txt
    python manage.py syncdb --noinput
    python manage.py migrate
    python manage.py createsuperuser
    python manage.py runserver

#### Bootstraping ####
* setup gmail as email backend

```
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = True
EMAIL_HOST_PASSWORD = 'PASSWORD'
EMAIL_HOST_USER = 'USERNAME@gmail.com'
```

* setup Google Analytics

```
GOOGLE_ANALYTICS_PROPERTY_ID = 'UA-12345678-1'
```

* Document your web application: start with **project_readme.md** and overwrite **README.md**

#### Deployment (nginx, gunicorn, supervisor, virtualenv) ####

* create a new repo (same name as 'mysite') on Bitbucket and push the new created project into it
* customize fabfile.py
```python
    env.repository = 'https://bitbucket.org/myname/myreponame'
    env.hosts = [HOST-SERVER-NAME-OR-IP, ]
    env.user = HOST-USER
    env.key_filename = ["ec2.pem", ]  # PEM file for Aamzon EC2 (optional)
    ...
    env.gunicorn_bind = "127.0.0.1:PORT"  # port for the app instance, e.g. 8100
    ...
    env.nginx_server_name = 'mysite.com'  # Only domain name, without 'www' or 'http://'
```

* run test, and (system level and project level) setup on local machine

```
fab app test_configuration
fab app setupsystem
fab app setupproject
```

* manually create (mysql) database and configure local_settings.py on the server

```
$ mysql -u root -p
mysql> CREATE DATABASE dbname CHARACTER SET utf8 COLLATE utf8_general_ci;
mysql> GRANT ALL ON dbname.* TO 'dbuser'@'localhost' IDENTIFIED BY 'dbpassword';
mysql> quit
```

* run deployment

```
fab app deploy
```

#### Extra Manual Setup (optional) ####

```
sudo chown -R django site_media
```

#### Compile Documents ####

```
cd \slate
bundle exec middleman server
```

#### Test ####

```
curl    --request GET -H "Authorization: Token 6d958f521c8a9faf282fb2441f05f14bbc2b48be"  http://127.0.0.1:8000/dataapp/api/users/player1/  | python -mjson.tool
```
